import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DrawerBoardComponent } from './pages/drawer-board/drawer-board/drawer-board.component';

export const appRoutes: Routes = [
    {path: '', component: DrawerBoardComponent},
    {path: 'drawer-board', component: DrawerBoardComponent},
    {path: '**', redirectTo: 'drawer-board', pathMatch: 'full'}
];