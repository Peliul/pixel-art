import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-drawer-board',
  templateUrl: './drawer-board.component.html',
  styleUrls: ['./drawer-board.component.scss']
})
export class DrawerBoardComponent implements OnInit {
  @ViewChild('pixelArt', {static: false}) pixelArt: ElementRef;
  @ViewChild('downloadLink', {static: false}) downloadLink: ElementRef;
  @ViewChild('canvas', {static: false}) canvas: ElementRef;

  grids: any[];
  colors: string[];
  selectedGrid = 0;

  rows: number[];
  columns: number[];
  color = 'white';


  constructor() { }

  ngOnInit() {
    this.grids = [
      {title: '8x8', rows: 8, columns: 8},
      {title: '12x12', rows: 12, columns: 12},
      {title: '16x16', rows: 16, columns: 16},
      {title: '32x32', rows: 32, columns: 32}
    ];

    this.colors = ['white', 'black', 'red', 'green', 'yellow', 'yellow', 'green', 'teal', 'lightgreen', 'orange', 'purple', 'pink',
      'Indigo', 'blue', 'lightblue', 'aqua', 'grey', 'brown', 'lightgrey'];

    this.rows = new Array(this.grids[this.selectedGrid].rows);
    this.columns = new Array(this.grids[this.selectedGrid].columns);
  }

  // changes grid size
  changeGrid(index) {
    this.selectedGrid = index;
    this.rows = new Array(this.grids[index].rows);
    this.columns = new Array(this.grids[index].columns);
  }

  fill(i, j) {
    const id = 'r' + i.toString() + 'c' + j.toString();
    console.log('id', id);
    document.getElementById(id).style.backgroundColor = this.color;
  }

  changeColor(color) {
    this.color = color;
  }

  reset() {
    for (let i = 0; i < this.rows.length; i++) {
      for (let j = 0; j < this.columns.length; j++) {
        const id = 'r' + i.toString() + 'c' + j.toString();
        document.getElementById(id).style.backgroundColor = 'white';
      }
    }
  }

  download1() {
    html2canvas(this.pixelArt.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'pixel-art.png';
      this.downloadLink.nativeElement.click();
    });
  }

  download() {
    html2canvas(this.pixelArt.nativeElement).then(canvas => {
      canvas.toBlob(blob => {
        const link = document.createElement('a');
        link.download = 'image.png';
        link.href = URL.createObjectURL(blob);
        link.click();
      }, 'image/png');
    });
  }
}
