# Task Result

## Used technologies / Frameworks

I use the following framework in my project:

- Typescript/Angular CLI

I chose Angular because is a strong framework for front-end wich uses Typescript, a language built around Javascript because it fully complies Javascript but Typescript have a better tooling, have a cleaner code and higher
scalability, so common mistakes in writing code have been reduced. Another reasons for using Angular are the benefits of this framework as reusability, readability and maintanability.
## Used 3rd Party Libraries

I use the following 3rd party libraries in my project:

Name | Reason
--- | ---
[Ngx-Bootstrap](https://valor-software.com/ngx-bootstrap/#/) | I used it to create a dropdown-menu design and for buttons styling. Ngx-Bootstrap provides a simple way to create dropdown-menus and others awesome templates.
[Html2Canvas] (https://html2canvas.hertzen.com) - I used it for image download functionality.

## Installation / Run

The following components must be installed locally:

- [nodejs](https://nodejs.org/en/) v12.14.1
- [Angular CLI] (https://cli.angular.io) v8.3.25

To run the project locally, enter the following in the command line / bash:

```console
$ git clone git@bitbucket.org:Peliul/pixel-art.git (SSH) / git clone https://Peliul@bitbucket.org/Peliul/pixel-art.git (HTTPS)
$ cd Pixel-Art
$ npm install
$ ng serve
$ type in browser https://localhost:4200
```
---
